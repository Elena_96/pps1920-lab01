package lab01.example.model;

public interface BankAccountWithAtm extends BankAccount {
    void depositWithAtm(final int usrID, final double amount);
    void withdrawWithAtm(final int usrID, final double amount);
}
